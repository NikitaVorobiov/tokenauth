/**
 * Created by hanni on 11/21/16.
 */
exports.routes = function routes (app, secureRouters) {

    var users = require('./components/users');
    var posts = require('./components/posts');

    // app.get('/posts', posts.getAllPosts);
    app.post('/signin', users.authenticationUser);
    secureRouters.post('/posts', posts.addNewPost);


//     app.get('/products', products.getAllProducts);
//     app.get('/products/:id', products.getProductById);
//     app.post('/products', products.addNewProduct);
//     app.put('/products/:id', products.updateProduct);
//     app.delete('/products/:id', products.deleteProduct);
};