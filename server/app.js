/**
 * Created by hanni on 11/21/16.
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var secureRouters = express.Router();
var jwt = require('jsonwebtoken');

app.use(bodyParser.json());
app.use('/secure-api', secureRouters)

app.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
});

secureRouters.use(function (req, res, next) {
    var token = req.body.token || req.headers['token'];

    if(token) {
        jwt.verify(token, process.env.SECRET_KEY, function (err, decode) {
            if (err) {
                res.status(500).send('Invalid token');
            } else {
                next();
            }
        });
    } else {
        res.send('Token is mandatory');
    }
});

require('./routes').routes(app, secureRouters);
// require('./routes').routes(secureRouters);

var server = app.listen('3333', function () {
    console.log('Server port: ' + server.address().port);
});