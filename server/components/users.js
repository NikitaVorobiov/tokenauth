/**
 * Created by hanni on 11/21/16.
 */
var MongoClient = require('mongodb').MongoClient;
// MongoClient.connect('mongodb://localhost:27017/users');
var jwt = require('jsonwebtoken');
process.env.SECRET_KEY = 'nikitkasecretkey';

exports.authenticationUser = function authenticationUser(req, res) {
    if(!req.body.login) {
        res.status(401).send('Login is mandatory');
    }
    if(!req.body.password) {
        res.status(401).send('Password is mandatory');
    }

    MongoClient.connect('mongodb://localhost:27017/users')
        .then(response => {
            var user = response.collection('users').find({login: req.body.login, password: req.body.password}).toArray();
            return user;
        })
        .then (user => {
            var token = jwt.sign(user[0], process.env.SECRET_KEY, {
                expiresIn: 4000

            });

            res.json({
                success:true,
                token:token
            });
        });

};

exports.checkInUser = function checkInUser(req, res) {

};